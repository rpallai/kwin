# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vit Pelcak <vit@pelcak.org>, 2013, 2014, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-01 00:48+0000\n"
"PO-Revision-Date: 2014-09-22 15:29+0200\n"
"Last-Translator: Vít Pelčák <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.0\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: genericscriptedconfig.cpp:70
#, kde-format
msgctxt "Error message"
msgid "Could not locate package metadata"
msgstr ""

#: genericscriptedconfig.cpp:84
#, kde-format
msgctxt "Required file does not exist"
msgid "%1 does not contain a valid metadata.json file"
msgstr ""

#: genericscriptedconfig.cpp:90 genericscriptedconfig.cpp:96
#, kde-format
msgctxt "Required file does not exist"
msgid "%1 does not exist"
msgstr ""
